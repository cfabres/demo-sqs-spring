**Spring SQS example**

The main idea is consume and produce messages thru SQS.

**compile**
`mvn clean install`

**execute**
cd target
`java -jar -Dmyapp.accesskey=<your access key> -Dmyapp.secretaccesskey=<your secreat access key> -Dmyapp.standardqueue=<url of the queue> demosqs-0.0.1-SNAPSHOT.jar `

**testing the service**
**insert a message**
+ [http://localhost:8080/insertMessage?message=here%20we%20go%20with%20the%20body%20of%20the%20message](http://localhost:8080/insertMessage?message=here%20we%20go%20with%20the%20body%20of%20the%20message)
+ description: it inserts a message with the body described in message variable

**insert a message with 30 seconds of delay**
+ [http://localhost:8080/insertMessageWithDelay?message=messageWithDelay](http://localhost:8080/insertMessageWithDelay?message=messageWithDelay)
+ description: it inserts a message with the body described in message variable and also with a delay of 30 seconds, that means will no be visible in the queue until 30 seconds happen.

**send batch messages**
+ [http://localhost:8080/sendBatchMessages?message=batchMessageBody](http://localhost:8080/sendBatchMessages?message=batchMessageBody)
+ it will send 10 messages in batch, all together.

**get a message**
+ [http://localhost:8080/getMessage](http://localhost:8080/getMessage)
+ it process a message.
+ message available will decrease in one, and in flight will increase in one.

**get messages**
+ [http://localhost:8080/getMessages](http://localhost:8080/getMessages)
+ it process 10 message as maximun.
+ message available will decrease in how many get, and in flight will increase in how many get.
+ the long polling will increased in 20 seconds (withWaitTimeSeconds method).

**get getMessageWith60SecondsVisibilityTimeOut**
+ [http://localhost:8080/getMessageWith60SecondsVisibilityTimeOut](http://localhost:8080/getMessageWith60SecondsVisibilityTimeOut)
+ samr as getMessage but increasing the visibility time out by 60 seconds (setVisibilityTimeout method).
+ message available will decrease in one by 60 seconds, and in flight will increase in one by 60 seconds.

**get a message and delete**
+ [http://localhost:8080/getMessageAndDelete](http://localhost:8080/getMessageAndDelete)
+ it process a message and then will delete from the queue.

**get message and delete them in batch**
+ [http://localhost:8080/getMessagesAndDeleteWith60SecondsVisibilityTimeOut](http://localhost:8080/getMessagesAndDeleteWith60SecondsVisibilityTimeOut)
+ it process messages and then delete them from the queue.
+ can process at maximum 10 messages, but could be less.

**Diagram**
![Repo List](img/diagram.png)