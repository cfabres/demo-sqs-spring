package com.example.demosqs;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;

@SpringBootApplication
public class DemosqsApplication {

	@Value("${myapp.accesskey}")
	private String accessKey;
	
	@Value("${myapp.secretaccesskey}")
	private String secretAccessKey;
	
	public static void main(String[] args) {
		SpringApplication.run(DemosqsApplication.class, args);
	}

	@Bean
	public AWSCredentials credentials() {
		return new BasicAWSCredentials(this.accessKey, this.secretAccessKey);
	}
	
	@Bean
	public AmazonSQS amazonsqs(AWSCredentials credentials) {
		return AmazonSQSClientBuilder.standard()
				  .withCredentials(new AWSStaticCredentialsProvider(credentials))
				  .withRegion(Regions.US_EAST_1)
				  .build();
	}
	
}
