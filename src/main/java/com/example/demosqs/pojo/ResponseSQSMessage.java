package com.example.demosqs.pojo;

public class ResponseSQSMessage {

	private String messageId;
	private String headerMD5;
	private String bodyMD5;
	
	public String getMessageId() {
		return messageId;
	}
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	public String getHeaderMD5() {
		return headerMD5;
	}
	public void setHeaderMD5(String headerMD5) {
		this.headerMD5 = headerMD5;
	}
	public String getBodyMD5() {
		return bodyMD5;
	}
	public void setBodyMD5(String bodyMD5) {
		this.bodyMD5 = bodyMD5;
	}
	
}
