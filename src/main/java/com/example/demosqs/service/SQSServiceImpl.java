package com.example.demosqs.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.DeleteMessageBatchRequest;
import com.amazonaws.services.sqs.model.DeleteMessageBatchRequestEntry;
import com.amazonaws.services.sqs.model.DeleteMessageBatchResult;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.DeleteMessageResult;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.MessageAttributeValue;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageBatchRequest;
import com.amazonaws.services.sqs.model.SendMessageBatchRequestEntry;
import com.amazonaws.services.sqs.model.SendMessageBatchResult;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageResult;

@Service
public class SQSServiceImpl {

	@Autowired
	private AmazonSQS sqs;
	
	/**
	 * send message immediately
	 * 
	 * @param bodyMessage
	 * @param queueUrl
	 * @return
	 */
	public SendMessageResult sendMessage(String bodyMessage, String queueUrl) {
		SendMessageRequest smr = this.buildMessage(bodyMessage, queueUrl);
		return sqs.sendMessage(smr);
	}
	
	/**
	 * send a message with 30 seconds of delay
	 * 
	 * @param bodyMessage
	 * @param queueUrl
	 * @return
	 */
	public SendMessageResult sendMessageWithDelay(String bodyMessage, String queueUrl) {
		SendMessageRequest smr = this.buildMessage(bodyMessage, queueUrl);
		smr.withDelaySeconds(30);
		return sqs.sendMessage(smr);
	}
	
	private SendMessageRequest buildMessage(String bodyMessage, String queueUrl) {
		Map<String, MessageAttributeValue> messageAttributes = new HashMap<>();
		messageAttributes.put("system_id", new MessageAttributeValue()
				  .withStringValue("SYSTEM_01")
				  .withDataType("String"));
		messageAttributes.put("DATE", new MessageAttributeValue()
				  .withStringValue( "" + System.currentTimeMillis() )
				  .withDataType("String"));
		     
		SendMessageRequest sendMessageStandardQueue = new SendMessageRequest()
				  .withQueueUrl(queueUrl)
				  .withMessageBody( bodyMessage )
				  .withMessageAttributes(messageAttributes);
		 
		return sendMessageStandardQueue;
	}
	
	
	/**
	 * send 10 messages at once
	 * 
	 * @param body
	 * @param queueUrl
	 * @return
	 */
	public SendMessageBatchResult send10Messages(String body, String queueUrl) {
		SendMessageBatchRequest smbr = this.build10Messages(body, queueUrl);
		return sqs.sendMessageBatch(smbr);
	}
	
	/**
	 * build 10 messages at once
	 * 
	 * @param body
	 * @param queueUrl
	 * @return
	 */
	private SendMessageBatchRequest build10Messages(String body, String queueUrl) {
		List <SendMessageBatchRequestEntry> messageEntries = new ArrayList<>();
		
		for(int i=0;i<10;i++) {
			SendMessageBatchRequestEntry smbre = new SendMessageBatchRequestEntry();
			smbre.setId(i+"");
//			smbre.setMessageGroupId("TheSameGroup"); //combined with groupID will respect the order just in fifo queue
			smbre.setMessageBody(body);
			messageEntries.add(smbre);
		}
		
		return new SendMessageBatchRequest(queueUrl, messageEntries);
	}
	
	/**
	 * get one message from the queue with the default visibility timeout (30 sencods)
	 * 
	 * @param queueUrl
	 * @return
	 */
	public List<Message> getMessage(String queueUrl) {
		ReceiveMessageRequest rmr = new ReceiveMessageRequest(queueUrl);
		List<Message> sqsMessages = sqs.receiveMessage(rmr).getMessages();
		for(Message message: sqsMessages) {
			System.out.println( "\tId:" + message.getMessageId() + 
					" Body:" + message.getBody());
		}
		return sqsMessages;
	}
	
	/**
	 * process and delete a message
	 * using the method getMessage
	 * 
	 * @param queueUrl
	 * @return
	 */
	public DeleteMessageResult getMessageAndDelete(String queueUrl) {
		List<Message> lmessage = this.getMessage(queueUrl);
		System.out.println( "deleting the message with the following ReceiptHandle: " + lmessage.get(0).getReceiptHandle() );
		return sqs.deleteMessage( new DeleteMessageRequest().withQueueUrl(queueUrl).withReceiptHandle( lmessage.get(0).getReceiptHandle() ) );
	}
	
	/**
	 * get one message from the queue with 60 seconds visibility timeout
	 * 
	 * @param queueUrl
	 * @return
	 */
	public List<Message> getMessageWith60SecondsVisibilityTimeOut(String queueUrl) {
		ReceiveMessageRequest rmr = new ReceiveMessageRequest(queueUrl);
		rmr.setVisibilityTimeout(60);
		List<Message> sqsMessages = sqs.receiveMessage(rmr).getMessages();
		for(Message message: sqsMessages) {
			System.out.println( "\tId:" + message.getMessageId() + 
					" Body:" + message.getBody());
		}
		return sqsMessages;
	}
	
	
	
	public List<Message> getMessages(String queueUrl) {
		ReceiveMessageRequest rmr = new ReceiveMessageRequest(queueUrl)
				  .withWaitTimeSeconds(20)		//this is long polling, max 20 seconds
				  .withMaxNumberOfMessages(10);	//max amount of messages, the limit is 10.
		List<Message> sqsMessages = sqs.receiveMessage(rmr).getMessages();
		for(Message message: sqsMessages) {
			System.out.println( "\tId:" + message.getMessageId() + 
					" Body:" + message.getBody());
		}
		return sqsMessages;
		}

	/**
	 * get 10 messages from the queue with 20 seconds visibility timeout
	 * then process and delete them.
	 * 
	 * @param queueUrl
	 * @return
	 */
	public DeleteMessageBatchResult getMessagesAndDeleteWith60SecondsVisibilityTimeOut(String queueUrl) {
		List<Message> lMessages = this.getMessages(queueUrl);
		
		List<DeleteMessageBatchRequestEntry> listDmbre = new ArrayList<DeleteMessageBatchRequestEntry>();
		for(Message message: lMessages) {
			System.out.println( "\tId:" + message.getMessageId() + 
					" ReceiptHandle:" + message.getReceiptHandle() );
			DeleteMessageBatchRequestEntry dmbre = new DeleteMessageBatchRequestEntry();
			dmbre.setId(message.getMessageId());
			dmbre.setReceiptHandle(message.getReceiptHandle());
			listDmbre.add(dmbre);
		}
		DeleteMessageBatchRequest dmbr = new DeleteMessageBatchRequest();
		dmbr.setQueueUrl(queueUrl);
		dmbr.setEntries(listDmbre);
		if (listDmbre!=null && listDmbre.size()>0) {
			return sqs.deleteMessageBatch(dmbr);
		}else {
			return null;
		}
	}
	
}