package com.example.demosqs.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.amazonaws.services.sqs.model.DeleteMessageBatchResult;
import com.amazonaws.services.sqs.model.DeleteMessageResult;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.SendMessageBatchResult;
import com.amazonaws.services.sqs.model.SendMessageResult;
import com.example.demosqs.service.SQSServiceImpl;

@RestController
public class HttpController {

	@Autowired
	private SQSServiceImpl sqsService;
	
	@Value("${myapp.standardqueue}")
	private String standarQueueUrl;
	
	@RequestMapping(value="/insertMessage", method=RequestMethod.GET)
	public SendMessageResult insertMessage(@RequestParam String message) {
		return this.sqsService.sendMessage(message, this.standarQueueUrl);
	}
	
	@RequestMapping(value="/insertMessageWithDelay", method=RequestMethod.GET)
	public SendMessageResult insertMessageWithDelay(@RequestParam(required=true) String message) {
		return this.sqsService.sendMessageWithDelay(message, this.standarQueueUrl);
	}

	@RequestMapping(value="/sendBatchMessages", method=RequestMethod.GET)
	public SendMessageBatchResult sendBatchMessages(@RequestParam(required=true) String message) {
		return this.sqsService.send10Messages(message, this.standarQueueUrl);
	}
	
	@RequestMapping(value="/getMessage", method=RequestMethod.GET)
	public List<Message> getMessage() {
		return this.sqsService.getMessage(this.standarQueueUrl);
	}
	
	@RequestMapping(value="/getMessages", method=RequestMethod.GET)
	public List<Message> getMessages() {
		return this.sqsService.getMessages(this.standarQueueUrl);
	}
	
	@RequestMapping(value="/getMessageWith60SecondsVisibilityTimeOut", method=RequestMethod.GET)
	public List<Message> getMessageWith60SecondsVisibilityTimeOut() {
		return this.sqsService.getMessageWith60SecondsVisibilityTimeOut(this.standarQueueUrl);
	}
	
	@RequestMapping(value="/getMessageAndDelete", method=RequestMethod.GET)
	public DeleteMessageResult getMessageAndDelete() {
		return this.sqsService.getMessageAndDelete(this.standarQueueUrl);
	}

	@RequestMapping(value="/getMessagesAndDeleteWith60SecondsVisibilityTimeOut", method=RequestMethod.GET)
	public DeleteMessageBatchResult getMessagesAndDeleteWith60SecondsVisibilityTimeOut() {
		return this.sqsService.getMessagesAndDeleteWith60SecondsVisibilityTimeOut(this.standarQueueUrl);
	}
	
}
